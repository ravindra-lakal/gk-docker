FROM debian:9.0

MAINTAINER Shanavas M<shanavas@disroot.org>

ADD ./gnukhata /gnukhata

#creating necessary users and giving the permissions.
RUN groupadd -g 110 postgres && useradd -u 105 gkadmin && useradd -u 106 -g 110 postgres

# installing required dependencies
RUN apt-get update && apt-get install -y \
    python2.7 \
    postgresql \
    python-pip \
    python-dev \
    libpq-dev \
    nginx

# clean package cache
RUN apt-get autoclean -y &&\
    apt-get autoremove -y

# install python dependencies
RUN pip install pyramid \
    pyramid_jinja2 \
    Babel \
    psycopg2 \
    odslib \
    sqlalchemy \
    waitress \
    supervisor \
    wsgicors \
    requests \
    monthdelta \
    pyjwt \
    openpyxl \
    pillow

COPY nginx.conf /etc/nginx/
COPY gnukhata.conf /etc/nginx/conf.d/

VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]
RUN apt-get autoremove -y && apt-get clean

EXPOSE 80

WORKDIR /gnukhata

CMD ["/bin/bash", "startscript.sh"]