#!/bin/bash
service postgresql start

echo "setting file permissions ..."
chown gkadmin /gnukhata/

echo "installing gnukhata core ....."
cd /gnukhata/gkcore
pip install -e .

echo "Running database migrations ..."
su -c "/bin/bash /gnukhata/gkutil.sh" postgres
su -c "python /gnukhata/gkcore/initdb.py" gkadmin

echo "installing gnukhata webapp ....."
cd /gnukhata/gkwebapp
pip install -e .

service nginx start
cd /gnukhata

su -c "supervisord" gkadmin
echo "GNUKhata is running.... "
echo "Here we go.... :)"

tail -f /dev/null
